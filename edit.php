<?php 
include "conf.php";

$id = $_GET['id'];

// Validate and sanitize the ID
$id = intval($id);

$sql = "SELECT * FROM user_details WHERE Id = ?";
$stmt = $conn->prepare($sql);
$stmt->bind_param("i", $id);
$stmt->execute();
$result = $stmt->get_result();

if ($result->num_rows == 1) {
    $row = $result->fetch_assoc();
    $firstname = $row["FirstName"];
    $lastname = $row["LastName"];
    $tel = $row["MobileNumber"];
    $dob = $row["Birthday"];
} else {
    echo "Record not found";
    exit();
}

$stmt->close();

if (isset($_POST['submit'])) {
    $new_firstname = $_POST['firstname'];
    $new_lastname = $_POST['lastname'];
    $new_tel = $_POST['tel'];
    $new_dob = $_POST['dob'];

    // Prepare and bind
    $update_sql = "UPDATE user_details SET FirstName = ?, LastName = ?, MobileNumber = ?, Birthday = ? WHERE Id = ?";
    $update_stmt = $conn->prepare($update_sql);
    $update_stmt->bind_param("ssssi", $new_firstname, $new_lastname, $new_tel, $new_dob, $id);

    if ($update_stmt->execute()) {
        header("Location: view2.php");
    } else {
        echo "Error updating record: " . $update_stmt->error;
    }

    $update_stmt->close();
}

mysqli_close($conn);
?>

<html lang="en">
<head>
    <title>Edit Record</title>
</head>
<body>
    <h2>Edit Record</h2>
    <form method="post" action="">
        First Name:<input type="text" id="firstname" name="firstname" value="<?php echo htmlspecialchars($firstname); ?>"><br>
        Last Name:<input type="text" id="lastname" name="lastname" value="<?php echo htmlspecialchars($lastname); ?>"><br>
        Mobile Number:<input type="tel" id="tel" name="tel" value="<?php echo htmlspecialchars($tel); ?>"><br>
        Birthday: <input type="date" id="dob" name="dob" value="<?php echo htmlspecialchars($dob); ?>"><br><br>     
        <input type="submit" name="submit" value="Update">
    </form>
</body>
</html>
