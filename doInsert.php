<?php

include "conf.php";

if (isset($_POST['submit'])) {
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $mobile = $_POST['contactno'];
    $dob = $_POST['dob'];

    // Prepare and bind
    $stmt = $conn->prepare("INSERT INTO user_details (FirstName, LastName, MobileNumber, Birthday) VALUES (?, ?, ?, ?)");
    $stmt->bind_param("ssss", $fname, $lname, $mobile, $dob);

    if ($stmt->execute()) {
        echo "New record created successfully";
    } else {
        echo "Error: " . $stmt->error;
    }

    $stmt->close();
} else {
    echo "No data received";
}

mysqli_close($conn);
?>
